#!/bin/sh

echo DEBUG=0 >> .env
echo SQL_ENGINE=django.db.backends.postgresql >> .env
echo DATABASE=postgres >> .env

echo SECRET_KEY=$SECRET_KEY >> .env
echo SQL_DATABASE=$SQL_DATABASE >> .env
echo SQL_USER=$SQL_USER >> .env
echo SQL_PASSWORD=$SQL_PASSWORD >> .env
echo SQL_HOST=$SQL_HOST >> .env
echo SQL_PORT=$SQL_PORT >> .env
echo FEASTLY_IMAGE=$IMAGE:feastly >> .env
echo NGINX_IMAGE=$IMAGE:nginx >> .env
echo INSTAPI_IMAGE=$IMAGE:instapi >> .env
echo UNDAPI_IMAGE=$IMAGE:underlords >> .env
echo CRYPTAPI_IMAGE=$IMAGE:cryptolio >> .env
echo TECHJ_IMAGE=$IMAGE:techjanitor >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN >> .env
echo CI_REGISTRY=$CI_REGISTRY >> .env
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env

echo POSTGRES_USER=$SQL_USER >> .env
echo POSTGRES_PASSWORD=$SQL_PASSWORD >> .env
echo POSTGRES_DB=$SQL_DATABASE >> .env
# echo POSTGRES_HOST=$SQL_HOST >> .env
# echo POSTGRES_PORT=$SQL_PORT >> .env