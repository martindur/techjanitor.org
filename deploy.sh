#!/bin/sh

ssh -o StrictHostKeyChecking=no $DO_USER@$DIGITAL_OCEAN_IP_ADDRESS << 'ENDSSH'
  cd docker-projects/techjanitor_org
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker pull $IMAGE:feastly
  docker pull $IMAGE:nginx
  docker pull $IMAGE:instapi
  docker pull $IMAGE:underlords
  docker pull $IMAGE:cryptolio
  docker pull $IMAGE:techjanitor
  docker-compose -f docker-compose.prod.yml up -d
ENDSSH